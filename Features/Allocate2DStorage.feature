Feature: Allocate Storage for 2D Barcodes

@Allocate2DScanProcess
Scenario: Allocate Storage Process
 Given The user is in Landing Screen
 When The user click on Allcoate Storage
 And The user navigates to Scanning screen
 Then The user scan the Allocation barcode
 And The user Navigates to item list screen
And The user scan the 2Dlist Barcode
      | Barcodes                                                                                                                                     |
      | JGB.610902090373929000000000DA200005001050618050618TPL1...WL778495754GB1......................................W104NR.9ZGB.G40BT....G40BT.... |                                                                                                                              
      | JGB.610902090373929000000000DA200005001050618050618TPL1...HL193910083GB45.....................................W104QP.9ZGB.G40BT....G40BT.... |                                                                                                                                                                                                                                                               |
      | JGB.610902090373929000000000DA200005001050618050618TPL1...TP034220093GB19.....................................W104RN.9ZGB.G40BT....G40BT.... |
 And The user book Allocate 
 
 @Allocate2DManualProcess
 Scenario: Allocate Manual Process
 Given The user is in Landing Screen
 When The user click on Allcoate Storage
 And The user navigates to Scanning screen
 And The user enter AllcoateStorage barcode manually  
 And Naivigate to allocate add items list add list manually  
 And The user book Allocate  
 

 