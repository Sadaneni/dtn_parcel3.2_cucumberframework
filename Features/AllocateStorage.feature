Feature: Allocate Storage

@AllocateScanProcess
Scenario: Allocate Storage Process
 Given The user is in Landing Screen
 When The user click on Allcoate Storage
 And The user navigates to Scanning screen
 Then The user scan the Allocation barcode
 And The user Navigates to item list screen
 And The user scan the list Barcode
      | Barcodes        |                                                                                                                             
      | HY194011033GB   |                                                                                                                               
      | WL901690142GB   |                                                                                                                                
      | FP092300065GB   |
 And The user book Allocate 
 
 @AllocateManualProcess
 Scenario: Allocate Manual Process
 Given The user is in Landing Screen
 When The user click on Allcoate Storage
 And The user navigates to Scanning screen
 And The user enter AllcoateStorage barcode manually  
 And Naivigate to allocate add items list add list manually  
 And The user book Allocate  
 
 @AllocateNonBarcodedItems
 Scenario: Allocate NonBarcoded Process
 Given The user is in Landing Screen
 When The user click on Allcoate Storage
 And The user navigates to Scanning screen
 Then The user scan the Allocation barcode
 And The user Navigate and click select NonBarcoded option
 And The user enter address and search 
 And The user selects the nonBarcoded items and confirm
 And The user book Allocate 
 