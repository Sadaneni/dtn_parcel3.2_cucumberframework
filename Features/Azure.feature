Feature: Azure Test

  @AzurePostBookIn
  Scenario: Azure VadidationPost
    Given the user fetch the details from the Database
      | Barcode      | BookInDateTime      | DeliveryBarcodeType | HouseNo | ItemBarcode | ItemBarcodeone |
      | Cs1234567890 | 06/11/2018 11:30:03 | P739                |      50 | 123456789GB |     4376549354 |
      

  @AzureGetp739Lookup
  Scenario: Azure VadidationGet
    Given the user tries to retrieve the barcode details and validate
      | Barcode       | ExpectedMessageID                    | ExpectedBarcode | ExpectedDeliveryBarcodeType |
      | CS543216896RM | c3af2f77-ed05-431a-8210-a476ede02018 | CS543216896RM   | P739                        |

  @AzurePostAddressLookup
  Scenario: Azure Validation Address look up service
    Given the user tries to post Address look up service with
      | HouseNo | PostalCode |
      |     100 | RR12TT     |

  @AzurePostBulkServicePost
  Scenario: Azure Validation Bulk Storage service
    Given the user tries to post a bulk storage service with
      | BarcodeOne     | BarcodeTwo     | ItemBarcodeOne | ItemBarcodeTwo | TrackingNumberOne | TrackingNumberTwo | MessageID                            | SelfLocationBarcode |
      | CS0000000509RM | CS1236547892RM | ZX123456789GB  | ZZ324563457GB  | ZX123456789GB     | ZZ324563457GB     | 08bd6598-dd25-4ded-9e48-1af1dfaa5cb2 |              000055 |

      
      