Feature: BookIn Process

  @BookInScanProcess
  Scenario: BookIn Scanning Process
    Given The user is in Landing Screen
    When The user click on BookIn Process
    And The user navigates to Scanning screen
    Then The user scan the barcode
    And The user Navigates to item list screen
    And The user scan the list Barcode
      | Barcodes                                                                                                                                     |
      | HY194011033GB                                                                                                                                |
      | WL901690142GB                                                                                                                                |
      | FP092300065GB                                                                                                                                |
   #   | JGB.610902090373929000000000DA200005001050618050618TPL1...TP034220093GB19.....................................W104RN.9ZGB.G40BT....G40BT.... |
    And The user delete the barcode
    And The user click on NextButton
    And The user navigate to back and handle datalost popup
    And The user scan the list Barcode
      | Barcodes      | 
      | FQ091720101GB | 
      | FQ091720146GB | 
    And The user click on NextButton
    And The user is in Address screen
    When The user enter the Address
    Then The user click on BookIn
    And The user verify BookIn process Text
    

  @BookInManualProcess
  Scenario: BookIn Manual Process
    Given The user is in Landing Screen
    When The user click on BookIn Process
    And The user navigates to Scanning screen
    And The user enter barcode P739 manually
    And Naivigate to add items list add list manually
    And The user click on NextButton
    And The user is in Address screen
    When The user enter the Address
    Then The user click on BookIn
    And The user verify BookIn process Text

  @LocalCollectScanProcess
  Scenario: LocalCollect Scanning Process
    Given The user is in Landing Screen
    When The user click on BookIn Process
    And The user navigates to Scanning screen
    And The user click on local collect Tab
    Then The user scan localcollect barcode
    And The user Navigates to item list screen
    And The user scan the list Barcode
      | Barcodes                                                                                                                                     |
      | HY194011033GB                                                                                                                                |
      | WL901690142GB                                                                                                                                |
      | FP092300065GB                                                                                                                                |
   #   | JGB.610902090373929000000000DA200005001050618050618TPL1...WL778495754GB1......................................W104NR.9ZGB.G40BT....G40BT.... |
    And The user delete the barcode
    And The user click on NextButton
    And The user is in Address screen
    When The user enter the Address
    Then The user click on BookIn
    And The user verify BookIn process Text

  @LocalCollectManualProcess
  Scenario: LocalCollect Manual Process
    Given The user is in Landing Screen
    When The user click on BookIn Process
    And The user navigates to Scanning screen
    And The user click on local collect Tab
    And The user enter barcode LocalCollect manually
    And Naivigate to add items list add list manually
    And The user click on NextButton
    And The user is in Address screen
    When The user enter the Address
    Then The user click on BookIn
    And The user verify BookIn process Text
