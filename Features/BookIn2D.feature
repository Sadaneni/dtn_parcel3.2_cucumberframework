Feature: BoonIn 2DScans

  @BookIn2DScanProcess
  Scenario: BookIn Scanning Process
    Given The user is in Landing Screen
    When The user click on BookIn Process
    And The user navigates to Scanning screen
    Then The user scan the barcode
    And The user Navigates to item list screen
    And The user scan the TwoDlist Barcode
      | Barcodes                                                                                                                                     |
      | JGB.610902090373929000000000DA200005001050618050618TPL1...WL778495754GB1......................................W104NR.9ZGB.G40BT....G40BT.... |
      | JGB.82152A03000001A770001200100390240815009....KB220000098GB....TW88FB...GB.TW88FB.S                                                         |
      | JGB 82141A0400237BA160000100100096241116006...............58..DA15FS...GB.EH87AW                                                             |
      | JGB 82152A03000001A770001200100390240815009..KB220000098GB....TW88FB...GB.TW88FB.S                                                           |
      | JGB 6109020A0167401000000774E3D00009511231116231116TPL1...WJ357162511GB....UNIT 12.THE BASE...................DA15FS.9ZGB.OX109TA..OX109TA   |
      | JGB.82141A0400237BA160000100100096241116006...............58..DA15FS.GB.EH87AW                                                               |
      | JGB.42122A0213B5C103CCA2D72431370000482100285231116132                                                                                       |
      | JGB.42122A0213B5C103CCA2D72431370000482100285231116132JGB.B1110101PB172874.00020390007806161115.............2F5038DCD5BC6A61                 |
    And The user delete the barcode
    And The user click on NextButton
    And The user navigate to back and handle datalost popup
    And The user scan the list Barcode
      | Barcodes      |
      | FQ091720101GB |
      | FQ091720146GB |
    And The user click on NextButton
    And The user is in Address screen
    When The user enter the Address
    Then The user click on BookIn
    And The user verify BookIn process Text

  @LocalCollect2DScanProcess
  Scenario: LocalCollect Scanning Process
    Given The user is in Landing Screen
    When The user click on BookIn Process
    And The user navigates to Scanning screen
    And The user click on local collect Tab
    Then The user scan localcollect barcode
    And The user Navigates to item list screen
    And The user scan the TwoDlist Barcode
      | Barcodes                                                                                                                                     |
      | JGB.610902090373929000000000DA200005001050618050618TPL1...WL778495754GB1......................................W104NR.9ZGB.G40BT....G40BT.... |
      | JGB.82152A03000001A770001200100390240815009....KB220000098GB....TW88FB...GB.TW88FB.S                                                         |
    And The user delete the barcode
    And The user delete the barcode
    And The user click on NextButton
    And The user is in Address screen
    When The user enter the Address
    Then The user click on BookIn
    And The user verify BookIn process Text
