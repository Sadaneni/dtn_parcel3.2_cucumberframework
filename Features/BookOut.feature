Feature: BookOut feature

@BookOutScanProcess
Scenario: BookOut Scanning process
Given The user is in Landing Screen
When The user click on BookOut process
And The user navigates to Scanning screen 
And The user scan the barcode
And The user verify the list and check check boxes are selected
Then The user do BookOut
Then The user clear the DataBase


@BookOutManulProcess
 Scenario: BookOut Manul Process
 Given The user is in Landing Screen
 When The user click on BookOut process
 And The user navigates to Scanning screen
 And The user enter barcode P739 manually
 And The user verify the list and check check boxes are selected
 Then The user do BookOut
 Then The user clear the DataBase

@BookOutSearchAddress
 Scenario: BookOut Search by Address
  Given The user is in Landing Screen
  When The user click on BookOut process
  And The user navigates to Scanning screen
  And The user click on address search tab
  And The user is in Address screen
  When The user enter the Address for bookout
  And The user search by address and select barcodes
  And The user verify the list and check check boxes are selected
  Then The user do BookOut
  Then The user clear the DataBase