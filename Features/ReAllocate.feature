Feature: ReAllocate Storage

@ReAllocateScan
  Scenario: ReAllcoate Process by scanning
    Given The user is in Landing Screen
    When The user click on ReAllcoate Item
    And The user navigates to Scanning screen
    Then The user scan the Allocation barcode
    And The user Navigates to item list screen
    And The user selects the items to reallocate
    And The user click on change location
    When The user scan new shelf Allocation barcode
    Then The user allocate new storage

 @ReAllocateManual   
    Scenario: ReAllcoate Process Manually
    Given The user is in Landing Screen
    When The user click on ReAllcoate Item
    And The user navigates to Scanning screen
    And The user enter AllcoateStorage barcode manually
    And The user Navigates to item list screen
    And The user selects the items to reallocate
    And The user click on change location
    When The user ReAllocate Barcode manually
    Then The user allocate new storage