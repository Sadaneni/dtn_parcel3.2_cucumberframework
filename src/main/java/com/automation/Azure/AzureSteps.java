package com.automation.Azure;

import java.io.IOException;
import java.util.List;

import org.apache.http.client.ClientProtocolException;

import com.automation.pages.AzureScreen;

import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import cucumber.api.java.en_lol.WEN;

public class AzureSteps {

	AzureScreen screen;
	
	@Given("the user fetch the details from the Database")
	public void theUserFetchTheDetailsFromTheDatabase(DataTable table) throws ClientProtocolException, IOException {
		screen = new AzureScreen();
		List<List<String>> data = table.raw();
		int value = data.size();
		for (int i = 1; i < value; i++) {
			String _Barcode = data.get(i).get(0);
			String _BookTime = data.get(i).get(1);
			String _DeliveryBarcodeType = data.get(i).get(2);
			String _houseno = data.get(i).get(3);
			String _Itemcode = data.get(i).get(4);
			String _ItemBarcodeone = data.get(i).get(5);
			screen.userConnectsRestApi(_Barcode, _BookTime, _DeliveryBarcodeType, _houseno, _Itemcode, _ItemBarcodeone);
		}	
		//screen.userConnectsRestApi(_Barcode, );
		
	}
	
	@Given("^the user tries to retrieve the barcode details and validate$")
	public void theUserTriesToRetrieveTheBarcodeDetailsAndValidate(DataTable table) throws ClientProtocolException, IOException {
		screen = new AzureScreen();
		List<List<String>> data = table.raw();
		int value = data.size();
		for (int i = 1; i < value; i++) {
			String _Barcode = data.get(i).get(0);
			String _ExpectedMessageID = data.get(i).get(1);
			String _ExpectedBarcode = data.get(i).get(2);
			String _ExpectedDeliveryBarcodeType  = data.get(i).get(3);
			
			screen.lookUpServiceApi(_Barcode, _ExpectedMessageID, _ExpectedBarcode, _ExpectedDeliveryBarcodeType);
			
		}	
		
	}
	
	
	@Given("the user tries to post Address look up service with")
	public void theUserTriesToPostAddressLookUpServiceWith(DataTable table) throws ClientProtocolException, IOException {
		screen = new AzureScreen();
		List<List<String>> data = table.raw();
		int value = data.size();
		for (int i = 1; i < value; i++) {
			String houseNo = data.get(i).get(0);
			String postalCode = data.get(i).get(1);
			screen.lookUpAddressService(houseNo, postalCode);
		}	
	}
	
	@Given("the user tries to post a bulk storage service with")
	public void theUserTriesToPostABulkStorageServiceWith(DataTable table) throws ClientProtocolException, IOException {
		screen = new AzureScreen();
		List<List<String>> data = table.raw();
		int value = data.size();
		for (int i = 1; i < value; i++) {
			String barcodeOne = data.get(i).get(0);
			String barCodeTwo = data.get(i).get(1);
			String itemBarcodeOne = data.get(i).get(2);
			String itemBarcodeTwo = data.get(i).get(3);
			String trackingNumberOne = data.get(i).get(4);
			String trackingNumberTwo = data.get(i).get(5);
			String messageId = data.get(i).get(6);
			String selfLocationbarcode = data.get(i).get(7);
			
			screen.postAllocationService(barcodeOne, barCodeTwo, itemBarcodeOne, itemBarcodeTwo, trackingNumberOne, trackingNumberTwo, messageId, selfLocationbarcode);
				
		}	
	}
}
