package com.automation.pages;

import java.util.List;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import junit.framework.Assert;

public class AddressEntryScreen extends Page{
	
	@AndroidFindBy(className="android.widget.ImageButton")
    public MobileElement backArrow;
	
	@AndroidFindBy(id="com.royalmail.csp.sms.dev:id/messageTextView")
	public MobileElement barcode;
	
	@AndroidFindBy(id="com.royalmail.csp.sms.dev:id/scannedItemsCountTextView")
	public MobileElement scanItemCount;

	@AndroidFindBy(id="com.royalmail.csp.sms.dev:id/postCodeEditText")
	public MobileElement postCode;

	@AndroidFindBy(id="com.royalmail.csp.sms.dev:id/houseNumberOrNameEditText")
	public MobileElement houseNo;

	@AndroidFindBy(id="com.royalmail.csp.sms.dev:id/dateClickableView")
	public MobileElement date;
	
	@AndroidFindBy(id="com.royalmail.csp.sms.dev:id/nonBarcodedItemsEditText")
	public MobileElement nonBarcodedItems;

	@AndroidFindBy(id="com.royalmail.csp.sms.dev:id/cancelTextViewButton")
	public MobileElement cancel;

	@AndroidFindBy(id="com.royalmail.csp.sms.dev:id/actionButton")
	public MobileElement bookIn;

	@AndroidFindBy(id="android:id/message")
	public MobileElement dataLostText;

	@AndroidFindBy(id="android:id/button2")
	public MobileElement cancelBarcode;

	@AndroidFindBy(id="android:id/button1")
	public MobileElement yesBarcode;

	@AndroidFindBy(id="com.royalmail.csp.sms.dev:id/alertTitle")
	public MobileElement nonBarcodedText;
	
	@AndroidFindBy(id="android:id/text1")
	public MobileElement nonbarcode;
	
	@AndroidFindBy(id="android:id/text1")
	public List<MobileElement> nonbarcodeOption;
	
	@AndroidFindBy(id="android:id/button1")
	public MobileElement confirmNonBarcode;

	public AddressEntryScreen(AppiumDriver<MobileElement> driver) {
		super(driver);
	}

	public Boolean assert_unKnownBarcodeText(){
		Boolean isunKnownBarcodeText= isElementVisible(barcode,driver);
		return isunKnownBarcodeText;
	}
    
	public void clickBackArrow(){
		backArrow.click();
	}
	public void enterPostcode(String postcode){
		postCode.sendKeys(postcode);
	}

	public void userSearchByAddress(){
		bookIn.click();
	}
	public void enterHouseNo(String HouseNo){
		houseNo.sendKeys(HouseNo);
		try{
			driver.hideKeyboard();
		}catch (Exception e){

		}
	}
	
	public void enterNonBarcodedItems(String nonBarcodes){
		nonBarcodedItems.sendKeys(nonBarcodes);
		try{
			driver.hideKeyboard();
		}catch (Exception e){

		}
	}
	public void clickBookIn(){
		bookIn.click();
	}

	public void clickDataLostYes(){
		yesBarcode.click();
	}
	public void clickDataLostNo(){
		cancelBarcode.click();
	}
	
	public void verifyNonBarcodedMessage(){
		String ExpectedText="Select number of non-barcoded items";
		String ActualText=nonBarcodedText.getText();
		Assert.assertEquals(ExpectedText, ActualText);
	}
	
	public void clickNonBarcoded(){
		 nonbarcode.click();
	}
	public void selectNonBarcodNumber(int index){
		nonbarcodeOption.get(index).click();
	}
	
	public void clickConfirmNonBarcoded(){
		confirmNonBarcode.click();
	}

	public void verifyDataLostMessage(){
		String ExpectedText="Data already entered will be lost. Are you sure you want to proceed?";
		String ActualText=dataLostText.getText();
		Assert.assertEquals(ExpectedText, ActualText);
	}
}
