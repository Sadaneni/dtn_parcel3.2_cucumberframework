package com.automation.pages;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import org.apache.http.HttpEntity;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Assert;

public class AzureScreen {
	
	public void userConnectsRestApi(String Barcode, String BookTime, String DeliveryBarcodeType, String houseno, String Itemcode, String _ItemBarcodeone) throws ClientProtocolException, IOException {
		CloseableHttpClient client = HttpClients.createDefault();
		HttpPost httpPost = new HttpPost("https://rmg-pda-csp-sms.azurewebsites.net/api/BookInService");
		
		String json = "{\r\n" + 
				"  \"barcode\": \"" + Barcode + "\",\r\n" + 
				"  \"bookInDateTime\": \"" + BookTime + " \",\r\n" + 
				"  \"deliveryType\": \"" + DeliveryBarcodeType +"\",\r\n" + 
				"  \"deviceID\": \"4a0b5617-26db-43bd-9888-79d72e925997\",\r\n" + 
				"  \"houseNo\": \""+ houseno +"\",\r\n" + 
				"  \"items\": [\r\n" + 
				"    {\r\n" + 
				"      \"itemBarcode\": \""+ Itemcode +"\",\r\n" + 
				"      \"manual\": false,\r\n" + 
				"      \"trackingNumber\": \"CS123456789GB\"\r\n" + 
				"    },\r\n" + 
				"    {\r\n" + 
				"      \"itemBarcode\": \"" + _ItemBarcodeone + "\",\r\n" + 
				"     \"manual\": false,\r\n" + 
				"      \"trackingNumber\": \"RM100000059GB\"\r\n" + 
				"    }\r\n" + 
				"  ],\r\n" + 
				"  \"itemsTotal\": 4,\r\n" + 
				"  \"manual\": false,\r\n" + 
				"  \"messageID\": \"c3af2f77-ed05-431a-8210-a476ede03005\",\r\n" + 
				"  \"postCode\": \"AB1 3GB\"\r\n" + 
				"}\r\n" + 
				"";
		
		StringEntity entity = new StringEntity(json);
		httpPost.setEntity(entity);	
	    httpPost.setHeader("Content-type", "application/json");
	    httpPost.setHeader("X-RMG-Message-ID", "c3af2f77-ed05-431a-8210-a476ede03006");
	    
	    CloseableHttpResponse response = client.execute(httpPost);
	    System.out.println(response.getStatusLine().getStatusCode());
	    Assert.assertEquals(200, response.getStatusLine().getStatusCode());
	    
	}
	
	public void lookUpServiceApi(String Barcode, String expectedMessageID, String expectedBarcode, String expectedDeliveryBarcodeType) throws ClientProtocolException, IOException {
		CloseableHttpClient client = HttpClients.createDefault();
		HttpGet httpGet = new HttpGet("https://rmg-pda-csp-sms.azurewebsites.net/api/lookupP739CLBarcode/" + Barcode + "");
		httpGet.setHeader("name", "james");
		
		
		CloseableHttpResponse response = client.execute(httpGet);
		System.out.println("The status line is  -->" + response.getStatusLine());
		
		HttpEntity entity = response.getEntity();
		String content = EntityUtils.toString(entity);
		
		
		//JSONObject jsonObj = new JSONObject(content);
		JSONArray jsonArray = new JSONArray(content);
		
		jsonArray.getJSONObject(0).getJSONObject("packageData").getString("DeliveryBarcodeType").compareTo(expectedDeliveryBarcodeType);
		
		jsonArray.getJSONObject(0).getJSONObject("packageData").getString("Barcode").compareTo(expectedBarcode);
				
	     jsonArray.getJSONObject(0).getJSONObject("packageData").getString("MessageID").compareTo(expectedMessageID);
		
	    
	}
	
	public void lookUpAddressService(String houseNumber, String postCode) throws ClientProtocolException, IOException {
		CloseableHttpClient client = HttpClients.createDefault();
		HttpPost httpPost = new HttpPost("https://rmg-pda-csp-sms.azurewebsites.net/api/LookupAddressBookinsP739s");
		String json = "{\r\n" + 
				" \"houseNo\": \"" + houseNumber + "\",\r\n" + 
				" \"postCode\": \""  + postCode + "\"\r\n" + 
				"}";
		
		StringEntity entity = new StringEntity(json);
		httpPost.setEntity(entity);	
	    httpPost.setHeader("Content-type", "application/json");
	    CloseableHttpResponse response = client.execute(httpPost);
	    System.out.println(response.getStatusLine().getStatusCode());
	    Assert.assertEquals(200, response.getStatusLine().getStatusCode());		
	}
	
	public void postAllocationService(String firstBarcode, String secondBarcode, String firstItemBarcode, String secondItemBarcode, String firstTrackingNumber, String secondTrackingNumber, String messageID, String selfLocatioBarcode ) throws ClientProtocolException, IOException {
		
		CloseableHttpClient client = HttpClients.createDefault();
		HttpPost httpPost = new HttpPost("https://rmg-pda-csp-sms.azurewebsites.net/api/BookBulkStorageService");
		String json = "{\r\n" + 
				"  \"items\": [\r\n" + 
				"    {\r\n" + 
				"      \"barcode\": \""+ firstBarcode + "\",\r\n" + 
				"      \"itemBarcode\": \"" + firstItemBarcode + "\",\r\n" + 
				"      \"manual\": false,\r\n" + 
				"      \"trackingNumber\": \"" + firstTrackingNumber + "\"\r\n" + 
				"    },\r\n" + 
				"    {\r\n" + 
				"      \"barcode\": \"" + secondBarcode + "\",\r\n" + 
				"      \"itemBarcode\": \"" + secondItemBarcode + "\",\r\n" + 
				"      \"manual\": false,\r\n" + 
				"      \"trackingNumber\": \"" + secondTrackingNumber + "\"\r\n" + 
				"    }\r\n" + 
				"  ],\r\n" + 
				"  \"manual\": true,\r\n" + 
				"  \"messageID\": \"" + messageID + "\",\r\n" + 
				"  \"shelfLocationBarcode\": \"" + selfLocatioBarcode + "\",\r\n" + 
				"  \"storageAllocationDateTime\": \"08/11/2018 12:00:46\",\r\n" + 
				"  \"storageEventType\": \"Allocation\"\r\n" + 
				"}";
		
		StringEntity entity = new StringEntity(json);
		httpPost.setEntity(entity);	
	    httpPost.setHeader("Content-type", "application/json");
	    CloseableHttpResponse response = client.execute(httpPost);		
	    System.out.println(response.getStatusLine().getStatusCode());
	    Assert.assertEquals(200, response.getStatusLine().getStatusCode());	
	}
	
	public void deAllocateService(){
		CloseableHttpClient client = HttpClients.createDefault();
		HttpPost httpPost = new HttpPost("https://rmg-pda-csp-sms.azurewebsites.net/api/BookBulkStorageService");
		
	}
	
	public void clearDataBase() throws ClientProtocolException, IOException{
		CloseableHttpClient client = HttpClients.createDefault();
		HttpPost get = new HttpPost("https://rmg-pda-csp-sms-utilfunc.azurewebsites.net/api/WipeDemoDB");
		CloseableHttpResponse response = client.execute(get);
		
	}
}
