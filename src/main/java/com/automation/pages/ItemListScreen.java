package com.automation.pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.pagefactory.AndroidFindBy;
import junit.framework.Assert;

import static io.appium.java_client.touch.offset.PointOption.point;
import static java.time.Duration.ofMillis;
import static io.appium.java_client.touch.WaitOptions.waitOptions;

public class ItemListScreen extends Page{
	
	@AndroidFindBy(className="android.widget.ImageButton")
	public MobileElement back;
	
	@AndroidFindBy(id="com.royalmail.csp.sms.dev:id/id_add_barcode_manually_menu")
	public MobileElement addIcon;
	
	@AndroidFindBy(id="com.royalmail.csp.sms.dev:id/messageTextView")
	public MobileElement p739BarcodeText;
	
	@AndroidFindBy(id="com.royalmail.csp.sms.dev:id/scannedItemsCountTextView")
	public MobileElement scanItemCount;
	
	@AndroidFindBy(className="android.view.View") //id="com.royalmail.csp.sms.dev:id/trackingNumberTextView"
	public List<MobileElement> barodeList;
	
	@AndroidFindBy(id="com.royalmail.csp.sms.dev:id/deleteItemLayout")
	public MobileElement delete;

	@AndroidFindBy(id="com.royalmail.csp.sms.dev:id/cancelTextViewButton")
	public MobileElement cancel;
	
	@AndroidFindBy(id="com.royalmail.csp.sms.dev:id/actionButton")
	public MobileElement enterAddress;
	
	@AndroidFindBy(id="com.royalmail.csp.sms.dev:id/snackbar_text")
	public MobileElement removeText;
	
	
	/**
	 *  Below id's are used for Allocate Storage 
	 *  **/
	
	@AndroidFindBy(className="android.widget.ImageView")
	public List<MobileElement> AllocationaddIcon;
	
	@AndroidFindBy(className="android.widget.LinearLayout")
	public List<MobileElement> addBarcode;
	
	@AndroidFindBy(id="com.royalmail.csp.sms.dev:id/id_dialog_barcode_edit_text")
	public MobileElement barcodeText;
	
	@AndroidFindBy(id="android:id/message")
	public MobileElement dataLostText;

	@AndroidFindBy(id="android:id/button2")
	public MobileElement cancelBarcode;
	
	@AndroidFindBy(id="android:id/button1")
	public MobileElement yesBarcode;
	
	/**
	 *  Below id's are used for ReAllocate Storage 
	 *  **/
	
	@AndroidFindBy(id="com.royalmail.csp.sms.dev:id/itemCollectedCheckBox")
	public List<MobileElement> checkBoxList;
	
	// Book Out
	@AndroidFindBy(id="com.royalmail.csp.sms.dev:id/itemCollectedCheckBox")
	public List<MobileElement> chekbox;
	
	public ItemListScreen(AppiumDriver<MobileElement> driver) {
		super(driver);
	}
	
	public void clickBack(){
		back.click();
	}
	
	public void clickCancel(){
		cancel.click();
	}
	
	public void clickEnterAddress(){
		enterAddress.click();
	}
	
	public void clickAddIcon(){
		addIcon.click();
	}
	
	public void clickAllocationAddIcon(int index){
		 AllocationaddIcon.get(index).click();
	}
	public void clickAddBarcode(int index){
		addBarcode.get(index).click();
	}
	public void enterBarcodeText(String BarcodeText){
		barcodeText.sendKeys(BarcodeText);
	}

	public void clickBarcodeTextYes(){
		yesBarcode.click();
	}
	
	public void clickDataLostYes(){
		yesBarcode.click();
	}
	public void clickDataLostNo(){
		cancelBarcode.click();
	}
	
	public void clickChangeLocation(){
		enterAddress.click();
	}
	
	public void clickBookOut(){
		enterAddress.click();
	}
	
	public void clickAllocate(){
		enterAddress.click();
	}
	
	
	public Boolean assert_P739Barcode(){
        Boolean isP739Barcode= isElementVisible(p739BarcodeText,driver);
        return isP739Barcode;
    }
	
	public void verifyDataLostMessage(){
		String ExpectedText="Data already entered will be lost. Are you sure you want to proceed?";
		String ActualText=dataLostText.getText();
		Assert.assertEquals(ExpectedText, ActualText);
	}
	public void verifyItemRemovedMessage(){
		String ExpectedText="Thank you, the item has been removed";
		String ActualText = removeText.getText();
		Assert.assertEquals(ExpectedText, ActualText);
	}
	public void deleteBarcode(){
		delete.click();
		yesBarcode.click();
	}
	 public void swipeLeft() throws InterruptedException{
		 new TouchAction(driver)
         .press(point(600,387))
         .waitAction(waitOptions(ofMillis(1000)))
         .moveTo(point(430, 379))
         .release().perform();
		  deleteBarcode();
		  Thread.sleep(3000);
	 }
	 
	public void emptyList() throws InterruptedException{
		Boolean isListEmpty= false;
		while(!isListEmpty){
			List<MobileElement> list = driver.findElements(By.id("com.royalmail.csp.sms.dev:id/trackingNumberTextView"));
			if ( list != null & list.size() > 0 ) { 
			isListEmpty = false;
			System.out.println("size = " + list.size() );
			list.get(0);
			swipeLeft();
		} 
		else{
			isListEmpty =true;
		}
			
		}
	}

	
	public void clickCheckBoxes(int index){
		chekbox.get(index).click();
	}
	
	public void verfiyCheckBoxesisSelected(int index){
		chekbox.get(index).isSelected();
	}
}
