package com.automation.pages;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import junit.framework.Assert;


public class MainScreen extends Page {

	@AndroidFindBy(className="android.widget.TextView")
	public MobileElement smsText;
	
	@AndroidFindBy(id="com.royalmail.csp.sms.dev:id/buttonBookIn")
	public MobileElement bookin;
	
	@AndroidFindBy(id="com.royalmail.csp.sms.dev:id/buttonAllocateSpace")
	public MobileElement allocatespace;
	
	@AndroidFindBy(id="com.royalmail.csp.sms.dev:id/buttonReallocateItems")
	public MobileElement reAllocatespace;
	
	@AndroidFindBy(id="com.royalmail.csp.sms.dev:id/buttonBookOut")
	public MobileElement bookout;
	
	@AndroidFindBy(id="com.royalmail.csp.sms.dev:id/snackbar_text")
	public MobileElement message;
	
	public MainScreen(AppiumDriver<MobileElement> driver) {
		super(driver);
	}

	 public Boolean assert_CspText(){
         Boolean isCspText= isElementVisible(smsText,driver);
         return isCspText;
     }
	public void tapOnBookIn(){
		bookin.click();
	}
	
	public void tapOnAllocateSpace(){
		allocatespace.click();
	}
	
	public void tapOnReAllocate(){
		reAllocatespace.click();
	}
	public void tapOnBookOut(){
		bookout.click();
	}
	public void verifyTheBookInprocessText(){
		String Actual ="Thank you, your item(s) are booked in";
		String Expected =message.getText();
		Assert.assertEquals(Expected, Actual);
	}
}
