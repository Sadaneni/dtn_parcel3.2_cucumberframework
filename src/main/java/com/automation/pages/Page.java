package com.automation.pages;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.time.Duration;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Point;
import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.touch.offset.PointOption;

public class Page extends PageObject {

	/**
	 * Constructor
	 * @param driver Appium driver for android
	 */
	public Page(AppiumDriver<MobileElement> driver)
    {
        super(driver);
        PageFactory.initElements(new AppiumFieldDecorator(driver, Duration.ofSeconds(60)), this);
    }

    public void doSendKeys(MobileElement textElement, String value)
    {
        textElement.click();
        textElement.clear();
        textElement.sendKeys(value);
        this.hideKeyboardIfPresent();
    }

    public void doSendKeysAndAutocomplete(MobileElement textElement, String value)
    {
        this.doSendKeysAndAutocompleteUsingIndex(textElement, value, 0);
    }

    public void doSendKeysAndAutocompleteUsingIndex(MobileElement textElement, String value, int index)
    {
        Point location = textElement.getLocation();
        this.doSendKeys(textElement, value);

        TouchAction ta0 = new TouchAction(driver);
        ta0.press(PointOption.point(location.x + 100, location.y + 100));
        ta0.perform();
        ta0.release();

        this.hideKeyboardIfPresent();
    }

    public void hideKeyboardIfPresent()
    {
        boolean isKeyboardPresent = false;

        try
        {
            Process p = null;

            p = Runtime.getRuntime().exec("adb shell dumpsys input_method | grep mInputShown");

            BufferedReader in = new BufferedReader(new InputStreamReader(p.getInputStream()));
            String outputText = "";

            while ((outputText = in.readLine()) != null)
            {

                if (!outputText.trim().equals(""))
                {
                    String keyboardProperties[] = outputText.split(" ");
                    String keyValue[] = keyboardProperties[keyboardProperties.length - 1].split("=");

                    String softkeyboardpresenseValue = keyValue[keyValue.length - 1];
                    if (softkeyboardpresenseValue.equalsIgnoreCase("false"))
                    {
                        isKeyboardPresent = false;
                    } else
                    {
                        isKeyboardPresent = true;
                    }
                }
            }
            in.close();
        } catch (IOException e)
        {
            e.printStackTrace();
        }

        if(isKeyboardPresent)
        {
            driver.hideKeyboard();
        }
    }
}