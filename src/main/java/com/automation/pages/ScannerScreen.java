package com.automation.pages;


import java.util.List;


import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;


public class ScannerScreen extends Page {

	@AndroidFindBy(className="android.widget.ImageButton")
	public MobileElement backarrow;

	@AndroidFindBy(id="android.widget.TextView")
	public MobileElement bookInText;

	@AndroidFindBy(id="com.royalmail.csp.sms.dev:id/id_add_barcode_manually_menu")
	public MobileElement addIcon;
	
	@AndroidFindBy(className="android.support.v7.app.ActionBar$Tab")
	public List<MobileElement>tab;

	@AndroidFindBy(id="com.royalmail.csp.sms.dev:id/scanBarcodeTextView")
	public MobileElement scanp739BarcodeText;
	
	@AndroidFindBy(id="com.royalmail.csp.sms.dev:id/id_dialog_barcode_edit_text")
	public MobileElement barcodeText;

	@AndroidFindBy(id="android:id/button2")
	public MobileElement cancelBarcode;
	
	@AndroidFindBy(id="android:id/button1")
	public MobileElement enterBarcode;
	
	//ReAllocation
	
	@AndroidFindBy(id="com.royalmail.csp.sms.dev:id/actionButton")
	public MobileElement allocatenewStorage;
	
	//BookOut
	
	public ScannerScreen(AppiumDriver<MobileElement> driver) {
		super(driver);	
	}
	
	public Boolean assert_BookInText(){
         Boolean isBookInText= isElementVisible(bookInText,driver);
         return isBookInText;
     }
	 
	
	public void clickAllocateNewStotage(){
		allocatenewStorage.click();
	}
	 public void clickOnLocalCollect(int index){
		 tab.get(index).click();
	 }
	 
	 public void clickOnSearchAddress(int index){
		 tab.get(index).click();
	 }

	public MainScreen clickOnBackArrow(){
		backarrow.click();
		return new MainScreen(driver);
	}
    
	public void clickOnAddIcon(){
		addIcon.click();
	}
	
	public void enterBarcodeText(String BarcodeText){
		barcodeText.sendKeys(BarcodeText);
	}
	
	public void clickBarcodeTextYes(){
		enterBarcode.click();
	}
	public void sendBarcode(String Barcode) {
		String barcodeQueryString = String.format("adb shell am broadcast -a com.royalmail.csp.sms.TEST_BARCODE_INTENT --es TEST_BARCODE_DATA  \"%S\"", Barcode );
		AdbBarcodeClient barcode = new AdbBarcodeClient();
		barcode.runADBCommand(barcodeQueryString);
	}
	
	public void send2DBarcode(String Barcode) {
		String barcodeQueryString1 = String.format("adb shell am broadcast -a com.royalmail.csp.sms.TEST_BARCODE_INTENT --es TEST_BARCODE_DATA \"%S\" --es TEST_BARCODE_TYPE_DATA \"2D\"", Barcode );
		AdbBarcodeClient barcode = new AdbBarcodeClient();
		System.out.println(barcodeQueryString1);
		barcode.runADBCommand(barcodeQueryString1);
		
	}

}
