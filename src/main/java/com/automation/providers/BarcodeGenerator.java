package com.automation.providers;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.apache.commons.lang.StringUtils;
import org.joda.time.DateTime;
import org.junit.Test;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.List;


public class BarcodeGenerator {

    /**
     * Given a prefix, cycles the given (serialised) json and return a new generated barcode
     *
     * @param _prefix
     * @return a generated barcode
     */
    public static String generateByPrefix(String _prefix) {
        String barcode = null;

        List<BarcodeObject> list = getList();
        for (BarcodeObject obj : list) {
            if (obj.Prefix.contains(_prefix)) {
                barcode = String.format("%s%s%s", obj.Prefix, getBarcodeDigits(), obj.Suffix);
                System.out.println(String.format("   >>> Product: %s SubProduct: %s Barcode: %s", obj.Product, obj.SubProduct, barcode));

                break;
            }
        }
        return barcode;
    }

    /**
     * Given product and subproduct, cycles the given (serialised) json and return a new generated barcode
     *
     * @param product
     * @param subProduct
     * @return a generated barcode
     */
    public static String generateByProductAndSubProduct(String product, String subProduct) {
        String barcode = null;

        List<BarcodeObject> list = getList();
        for (BarcodeObject obj : list) {
            if (obj.Product.contains(product) && obj.SubProduct.contains(subProduct)) {
                barcode = String.format("%s%s%s", obj.Prefix, getBarcodeDigits(), obj.Suffix);
                System.out.println(String.format("   >>> Product: %s SubProduct: %s Barcode: %s", obj.Product, obj.SubProduct, barcode));

                break;
            }
        }
        if (barcode == null) {
            System.err.println(String.format("##### !!! BARCODE NULL FOR PRODUCT: %s SUBPRODUCT: %s", product, subProduct));
        }
        return barcode;
    }

    /**
     * Given a product, cycles the given (serialised) json and return a new generated barcode
     *
     * @param product
     * @return a generated barcode
     */
    public static String generateBarcodeByProduct(String product) {

        String barcode = null;

        List<BarcodeObject> list = getList();
        for (BarcodeObject obj : list) {
            if (obj.Product.contains(product)) {
                barcode = String.format("%s%s%s", obj.Prefix, getBarcodeDigits(), obj.Suffix);
                System.out.println(String.format("   >>> Product: %s SubProduct: %s Barcode: %s", obj.Product, obj.SubProduct, barcode));

                break;
            }
        }
        return barcode;
    }

    /**
     * Return a list of BarcodeObject represented by barcodePrefixList.json
     *
     * @return barcodePrefixList.json into java BarcodeObjects
     */
    private static List<BarcodeObject> getList() {
        List<BarcodeObject> list = null;
        try {
            String resourcePath = "src/main/resources/barcodePrefixList.json";
            String content = new String(Files.readAllBytes(Paths.get(resourcePath)));

            list = new Gson().fromJson(content, new TypeToken<List<BarcodeObject>>() {
            }.getType());
            Collections.shuffle(list);

        } catch (FileNotFoundException fnfe) {
            fnfe.printStackTrace();
        } catch (UnsupportedEncodingException uee) {
            uee.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return list;
    }

    /**
     * Function to generate the 8 digits plus 1 check digit
     * Actually it takes in input a long timestamp and take a substring of 8 digits to create a sort of uniqueness
     *
     * @return fresh generated barcode digits
     */
    private static String getBarcodeDigits() {
        String generatedDigits = "";

        int[] weights = {8, 6, 4, 2, 3, 5, 9, 7};

        String randomBarcode = StringUtils.right("" + DateTime.now().getMillis(), 8);
        char[] randomBarcodeToChat = randomBarcode.toCharArray();

        int barcodeWeight = 0;
        for (int i = 0; i < randomBarcodeToChat.length; i++) {
            barcodeWeight += randomBarcodeToChat[i] * weights[i];
        }
        //
        int checkDigit;
        checkDigit = 11 - (barcodeWeight % 11);
        if (checkDigit == 10) {
            checkDigit = 0;
        }
        if (checkDigit == 11) {
            checkDigit = 5;
        }

        generatedDigits = randomBarcode.concat("" + checkDigit);
        //
        return generatedDigits;
    }

    @Test
    public void TestBarcodeGenerationByProduct() {
        System.out.println("BarcodeGenerator.generateBarcodeByProduct(): " + BarcodeGenerator.generateBarcodeByProduct("International Tracked"));
    }

    @Test
    public void TestBarcodeGenerationByPrefix() {
        System.out.println("TestBarcodeGenerationByPrefix(): " + BarcodeGenerator.generateByPrefix("RZ"));
        System.out.println("TestBarcodeGenerationByPrefix(): " + BarcodeGenerator.generateByPrefix("RH"));
        System.out.println("TestBarcodeGenerationByPrefix(): " + BarcodeGenerator.generateByPrefix("RI"));
        System.out.println("TestBarcodeGenerationByPrefix(): " + BarcodeGenerator.generateByPrefix("UJ"));
        System.out.println("TestBarcodeGenerationByPrefix(): " + BarcodeGenerator.generateByPrefix("CA"));
        System.out.println("TestBarcodeGenerationByPrefix(): " + BarcodeGenerator.generateByPrefix("PK"));
    }

    @Test
    public void TestBarcodeGenerationByProductAndSubProduct() {
        System.out.println("> ProductAndSubProduct: " + BarcodeGenerator.generateByProductAndSubProduct("Royal Mail Tracked 24", "RM Tracked 24 Non Signature"));
        System.out.println("> ProductAndSubProduct: " + BarcodeGenerator.generateByProductAndSubProduct("Royal Mail Tracked 24", "Royal Mail Tracked 24 Non Signature"));
        // International Parcels         | Inward Transit Express
        System.out.println("> ProductAndSubProduct: " + BarcodeGenerator.generateByProductAndSubProduct("International Parcels", "Inward Transit Express"));

    }

    // ---
    class BarcodeObject {
        public String Prefix;
        public String Suffix;
        public String Product;
        public String SubProduct;
        // following are checks
        public int Signature;
        public int Neighbour;
        public int Neighbour_Signature;
        public int Safeplace;
    }
}