package com.automation.providers;

import com.opencsv.CSVWriter;

import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;

public class PdaDetailList {

    public static String csvFilename = String.format("C:/PDAST-AutomationTest/data-regression-%s.csv", System.currentTimeMillis());
    private static PdaDetailList instance;
    private LinkedList<PdaDetail> pdaDetails;

    private PdaDetailList() {
        this.pdaDetails = new LinkedList<>();
    }

    public static synchronized PdaDetailList getInstance() {
        if (instance == null) {
            synchronized (PdaDetailList.class) {
                if (instance == null) {
                    instance = new PdaDetailList();
                }
            }
        }
        return instance;
    }

    public void addPdaDetail(String barcde, String eventCode,String deviceId) {
        this.pdaDetails.add(new PdaDetail(barcde, eventCode,deviceId));
    }

    public LinkedList<PdaDetail> getPdaDetails() {
        return this.pdaDetails;
    }

    public void writeCsvValues() {
        CSVWriter csvWriter = null;

        try {
            csvWriter = new CSVWriter(new FileWriter(PdaDetailList.csvFilename));
            String[] headline = {"barcode", "event_code", "timestamp", "device_id"};
            csvWriter.writeNext(headline);

            for (PdaDetailList.PdaDetail e : this.pdaDetails) {
                // timestamp format 2018-05-08 15:56:03.307
                String[] nextline = {e.barcode, e.eventCode, new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(e.timestamp),e.deviceId};
                csvWriter.writeNext(nextline);
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                csvWriter.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void writeCsvValues(String csvLocation, LinkedList<String[]> csvList) {
        CSVWriter csvWriter = null;

        try {
            csvWriter = new CSVWriter(new FileWriter(csvLocation));
            csvWriter.writeAll(csvList);

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                csvWriter.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public class PdaDetail {

        public String barcode;
        public String eventCode;
        public Date timestamp;
        public String deviceId;

        public PdaDetail(String barcode, String eventCode, String deviceId) {
            this.barcode = barcode;
            this.eventCode = eventCode;
            this.deviceId = deviceId;
            // workaround for server
            Calendar cal = Calendar.getInstance();
            cal.add(Calendar.HOUR, -1);
            this.timestamp = cal.getTime();
        }

    }

}
