package com.automation.steps;

import com.automation.pages.AddressEntryScreen;
import com.automation.pages.ItemListScreen;
import com.automation.pages.MainScreen;
import com.automation.pages.ScannerScreen;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;

public class AllocateSteps {
	
	protected final AppiumDriver<MobileElement>driver;
	 String AllOCATEBARCODE="000077";
	 String ITEMLIST="WL408534505";
	MainScreen main;
	ScannerScreen screen;
	ItemListScreen list;
	AddressEntryScreen entryscreen;
	
	public AllocateSteps(){
		this.driver= Hook.getDriver();
	}

	@When("^The user click on Allcoate Storage$")
	public void userTapAllcotionStorage(){
		main = new MainScreen(driver);
		main.tapOnAllocateSpace();
	}
	@Then("^The user scan the Allocation barcode$")
	public void scanAllocationBarcode(){
		screen = new ScannerScreen(driver);
		screen.sendBarcode(AllOCATEBARCODE);
	}
	
	@And("^The user Navigate and click select NonBarcoded option$")
	public void userSelectNonBarcodeOption() throws InterruptedException{
		list = new ItemListScreen(driver);
		list.clickAllocationAddIcon(0);
		list.clickAddBarcode(1);
	}
	
	@When("^The user enter AllcoateStorage barcode manually$")
	public void userEnterAllocateBarcodeManually(){
		screen = new ScannerScreen(driver);
		screen.clickOnAddIcon();
		screen.enterBarcodeText(AllOCATEBARCODE);
		screen.clickBarcodeTextYes();
	}

	
	@And("^Naivigate to allocate add items list add list manually$")
	public void addAllocatelistManually() throws InterruptedException{
		list = new ItemListScreen(driver);
		list.clickAllocationAddIcon(0);
		list.clickAddBarcode(0);
		list.enterBarcodeText(ITEMLIST);
		list.clickBarcodeTextYes();
		Thread.sleep(3000);
	}
	@And("^The user book Allocate$")
	public void userBookAllocate(){
		list = new ItemListScreen(driver);
		list.clickAllocate();
	}
}
