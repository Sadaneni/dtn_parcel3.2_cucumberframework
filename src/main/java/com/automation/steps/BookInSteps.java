package com.automation.steps;

import java.io.IOException;
import java.util.List;

import org.apache.http.client.ClientProtocolException;

import com.automation.pages.AddressEntryScreen;
import com.automation.pages.AzureScreen;
import com.automation.pages.ItemListScreen;
import com.automation.pages.MainScreen;
import com.automation.pages.ScannerScreen;
import com.automation.providers.PdaDetailList;

import cucumber.api.DataTable;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.Activity;
import io.appium.java_client.android.AndroidDriver;

public class BookInSteps {
	String Postcode="IG117GJ";
	String HouseNo="35";
	String NONBARCODEDITEMS="3";		
    String BARCODE="CS0000004478RM";
    String BARCODETEXT="0000000297";
    String ITEMLIST="WL408534505";
    String LOCALCOLLECTBARCODE="HY4087543457GB";
    String  LOCALCOLLECTMANUALLY="HY40873467GB";
	protected AppiumDriver<MobileElement>driver;
	MainScreen main;
	ScannerScreen screen;
	ItemListScreen list;
	AddressEntryScreen address;
	AzureScreen azure;
	
	public BookInSteps(){
		this.driver= Hook.getDriver();
	}
	
	@Given("^The user is in Landing Screen$")
	public void userIsInLandingScreen(){
		main = new MainScreen(driver);
		main.assert_CspText();
	}
	@And("^The user switch to ron App$")
	public void ronApp(){
		 Activity activity = new Activity("com.royalmail.lat", "md5f98083069ecbb5bb4b8e5195588163a4.ManifestActivity");
	        activity.setStopApp(false);
	        ((AndroidDriver<MobileElement>) driver).startActivity(activity);
	}
	@When("^The user click on BookIn Process$")
	public void userTapOnBookIn(){
		main = new MainScreen(driver);
		main.tapOnBookIn();
	}
	
	@And("^The user navigates to Scanning screen$")
	public void userIsInScanningScreen(){
		screen = new ScannerScreen(driver);
	//	screen.assert_BookInText();
	}
	
	@Then("^The user scan the barcode$")
	public void the_user_scan_the_p_barcode() throws Throwable {
		screen = new ScannerScreen(driver);
		screen.sendBarcode(BARCODE);
	}
	
	@And("^The user Navigates to item list screen$")
	public void userNaviageToItemlist(){
		list = new ItemListScreen(driver);
		list.assert_P739Barcode();
	}
	
	@And("^The user scan the list Barcode$")
	public void userScanListBarcode(DataTable table) throws InterruptedException{
		screen = new ScannerScreen(driver);
		List<List<String>> data = table.raw();
		int value = data.size();
		for (int i = 1; i < value; i++) {
		String Barcode = data.get(i).get(0);
		screen.sendBarcode(Barcode);
		Thread.sleep(2000);
		}
	}
	

	
	@And("^The user scan the TwoDlist Barcode$")
	public void userScan2DBarcodes(DataTable table){
		screen = new ScannerScreen(driver);
		List<List<String>> data = table.raw();
		int value = data.size();
		for (int i = 1; i < value; i++) {
		String Barcode = data.get(i).get(0);
		screen.send2DBarcode(Barcode);
		}
	}
	
	@And("^The user delete the barcode$")
	public void deleteTheBarcode() throws InterruptedException{
		list = new ItemListScreen(driver);
		list.swipeLeft();
	//	list.emptyList();
	}
	
	@And("^The user verfity the Removed SnackBar Text$")
	public void verifyRemoveText(){
		list = new ItemListScreen(driver);
		list.verifyItemRemovedMessage();
	}
	@And("^The user verify BookIn process Text$")
	public void verifyBookInText(){
		main = new MainScreen(driver);
		main.verifyTheBookInprocessText();
	}
	
	@And("^The user click on NextButton$")
	public void userClickOnNext(){
		list = new ItemListScreen(driver);
		list.clickEnterAddress();
	}
	
	
	@And("^The user navigate to back and handle datalost popup$")
	public void userNavigatesAndHandleDataLost(){
		address = new AddressEntryScreen(driver);
		address.clickBackArrow();
		address.verifyDataLostMessage();
		address.clickDataLostYes();
	}
	
	@And("^The user is in Address screen$")
	public void userIsInAddressScreen(){
		address = new AddressEntryScreen(driver);
		address.assert_unKnownBarcodeText();
	}
	
	@When("^The user enter the Address$")
	public void userEnterAddress(){
		address = new AddressEntryScreen(driver);
		address.enterPostcode(Postcode);
		address.enterHouseNo(HouseNo);
		address.enterNonBarcodedItems(NONBARCODEDITEMS);
	}
	
	@Then("^The user click on BookIn$")
	public void userClickOnBookIn(){
		address = new AddressEntryScreen(driver);
		address.clickBookIn();
	}
	@When("^The user enter barcode P739 manually$")
	public void userEnterBarcodeManually(){
		screen = new ScannerScreen(driver);
		screen.clickOnAddIcon();
		screen.enterBarcodeText(BARCODETEXT);
		screen.clickBarcodeTextYes();
	}
	
	@And("^Naivigate to add items list add list manually$")
	public void addlistManually() throws InterruptedException{
		list = new ItemListScreen(driver);
		list.clickAddIcon();
		list.enterBarcodeText(ITEMLIST);
		list.clickBarcodeTextYes();
		Thread.sleep(3000);
	}
	
	@Then("^The user scan localcollect barcode$")
	public void scanLocalCollectBarcode(){
		screen = new ScannerScreen(driver);
		screen.sendBarcode(LOCALCOLLECTBARCODE);
	}
	@And("^The user click on local collect Tab$")
	public void clickOnLocalCollectTab(){
		screen = new ScannerScreen(driver);
	    screen.clickOnLocalCollect(1);
	}
	@When("^The user enter barcode LocalCollect manually$")
	public void userEnterLocalBarcodeManually(){
		screen = new ScannerScreen(driver);
		screen.clickOnAddIcon();
		screen.enterBarcodeText(LOCALCOLLECTMANUALLY);
		screen.clickBarcodeTextYes();
	}
	
	
	@And("^The user enter address and search$")
	public void userEnterAddressAndSearch() throws InterruptedException{
		address = new AddressEntryScreen(driver);
		address.enterPostcode(Postcode);
		address.enterHouseNo(HouseNo);
		address.clickBookIn();
	}
	
	@And("^The user selects the nonBarcoded items and confirm$")
	public void userSelectsNonBarcode() throws InterruptedException{
		address = new AddressEntryScreen(driver);
		address.clickNonBarcoded();
		address.selectNonBarcodNumber(3);
		address.clickConfirmNonBarcoded();
		Thread.sleep(2000);
	}
	
	@And("^The user select check boxes$")
	public void userSelectCheckBoxes(){
		list = new ItemListScreen(driver);
		list.clickCheckBoxes(0);
		list.clickCheckBoxes(1);
		list.clickCheckBoxes(1);
		list.clickCheckBoxes(3);
	}
	
	@Then("^The user clear the DataBase$")
	public void clearDataBase() throws ClientProtocolException, IOException{
		azure = new AzureScreen();
		azure.clearDataBase();
	}
	
}
