package com.automation.steps;

import com.automation.pages.AddressEntryScreen;
import com.automation.pages.ItemListScreen;
import com.automation.pages.MainScreen;
import com.automation.pages.ScannerScreen;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;

public class BookOutSteps {
	
	protected AppiumDriver<MobileElement>driver;
	String Postcode="IG117GJ";
	String HouseNo="30";
	String NONBARCODEDITEMS="3";
	MainScreen main;
	ScannerScreen screen;
	ItemListScreen list;
	AddressEntryScreen address;
	
   public BookOutSteps(){
	   this.driver=Hook.getDriver();
   }

	@When("^The user click on BookOut process$")
	public void userTapOnBookOut(){
		main = new MainScreen(driver);
		main.tapOnBookOut();
	}
	
	@And("^The user verify the list and check check boxes are selected$")
	public void userSelectCheckBoxes(){
		list = new ItemListScreen(driver);
		list.verfiyCheckBoxesisSelected(0);
		list.verfiyCheckBoxesisSelected(1);
		list.verfiyCheckBoxesisSelected(2);
	}
	
	@Then("^The user do BookOut$")
	public void userBookOut(){
		list = new ItemListScreen(driver);
		list.clickBookOut();
	}
	
	@And("^The user click on address search tab$")
	public void clickOnAddressSearch(){
		screen = new ScannerScreen(driver);
		screen.clickOnSearchAddress(1);
	}
	
	@When("^The user enter the Address for bookout$")
	public void enterAddressToSearch(){
		address = new AddressEntryScreen(driver);
		address.enterPostcode(Postcode);
		address.enterHouseNo(HouseNo);
		address.userSearchByAddress();
	}
	@And("^The user search by address and select barcodes$")
	 public void selectsBarcodes() throws InterruptedException{
		address = new AddressEntryScreen(driver);
	    address.clickNonBarcoded();
		address.selectNonBarcodNumber(3);
		address.clickConfirmNonBarcoded();
		Thread.sleep(2000);
		
	}
}
