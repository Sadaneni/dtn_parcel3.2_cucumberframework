package com.automation.steps;

import com.automation.providers.AppiumConfigurationProvider;
import com.automation.providers.PdaDetailList;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.AndroidMobileCapabilityType;
import io.appium.java_client.remote.MobileCapabilityType;
import io.appium.java_client.remote.MobilePlatform;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.io.IOException;
import java.net.URL;
import java.sql.Timestamp;
import java.util.*;
import java.util.concurrent.TimeUnit;

public class Hook {

    protected static AppiumDriver<MobileElement> driver = null;

    /**
     * Return current instance of appium driver
     */
    public static AppiumDriver<MobileElement> getDriver() {
        return driver;
    }

    @Before
    public void setUp() throws IOException {
        DesiredCapabilities cap = new DesiredCapabilities();
        cap.setCapability(MobileCapabilityType.PLATFORM_NAME, AppiumConfigurationProvider.platformName());
        cap.setCapability(MobileCapabilityType.DEVICE_NAME, AppiumConfigurationProvider.deviceName());
   //     cap.setCapability(MobileCapabilityType.APP, AppiumConfigurationProvider.appAbsolutePath());
        cap.getCapability(MobileCapabilityType.PLATFORM_NAME).equals(MobilePlatform.ANDROID);  // platform is android
        cap.setCapability(AndroidMobileCapabilityType.APP_PACKAGE, AppiumConfigurationProvider.appPackage());
        cap.setCapability(AndroidMobileCapabilityType.APP_ACTIVITY, AppiumConfigurationProvider.appActivity());
        cap.setCapability(AndroidMobileCapabilityType.SUPPORTS_APPLICATION_CACHE, "false");
        cap.setCapability(MobileCapabilityType.NO_RESET, true);
        cap.setCapability(MobileCapabilityType.FULL_RESET, false);
        driver = new AndroidDriver<MobileElement>(new URL(AppiumConfigurationProvider.url()), cap);

        // set implicit timeout for @FindBy
        driver.manage().timeouts().implicitlyWait(AppiumConfigurationProvider.implicitTimeout(), TimeUnit.SECONDS);
        // #UNCOMMENT BELOW to create screenshots in c:/screenshots/
        // driver = EventFiringWebDriverFactory.getEventFiringWebDriver(driver, new EventListener());

        // --- printTelemetry -----
//        this.printTelemetry();
        // --- /printTelemetry -----
        //
 //       PdaDetailList.getInstance();
    }

    /**
     * This method is executed whenever a  test is completed
     */
    @After
    public void tearDown(Scenario scenario) {
        //  if (scenario.isFailed()) {
        scenario.embed(driver.getScreenshotAs(OutputType.BYTES), "image/png");
        //       }

        // --- printTelemetry -----
  //      this.printTelemetry();
        // --- /printTelemetry -----
        
        if (driver != null) {
            driver.resetApp();
            driver.closeApp();
            driver.quit();
        }

  //      PdaDetailList.getInstance().writeCsvValues();
    }

    private void printTelemetry()
    {
        LinkedList<String[]> csvbuffer = new LinkedList<>();
        AndroidDriver androidDriver = (AndroidDriver) driver;
        List<String> performanceTypes = androidDriver.getSupportedPerformanceDataTypes();
        long timestamp = System.currentTimeMillis();

        try {
            String[] rowTimestamp = {"Timestamp:", "'" + new Timestamp(timestamp).toString() + "'", " "};
            csvbuffer.add(rowTimestamp);

            for (String performanceType : performanceTypes) {
                // System.out.println("\t performanceType: " + timestamp);

                String[] rowType = {performanceType, " ", " "};
                csvbuffer.add(rowType);

                HashMap<String, String> memoryInfo = getMemoryInfo(androidDriver, performanceType);

                Iterator it = memoryInfo.entrySet().iterator();
                while (it.hasNext()) {
                    Map.Entry pair = (Map.Entry) it.next();

                    // System.out.println("\t\t " + pair.getKey() + " = " + pair.getValue());
                    String[] rowDetail = {" ", "" + pair.getKey(), "" + pair.getValue()};
                    csvbuffer.add(rowDetail);

                    it.remove(); // avoids a ConcurrentModificationException
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            String csvFilename = String.format("C:/PDAST-AutomationTest/APK-Performance-%s.csv", timestamp);
            PdaDetailList.getInstance().writeCsvValues(csvFilename, csvbuffer);
            System.out.println("\t APK-Performance created");
        }
    }

    private HashMap<String, String> getMemoryInfo(AndroidDriver driver, String performanceType) throws Exception {

        List<List<Object>> data = driver.getPerformanceData("com.royalmail.pda", performanceType, 10);

        HashMap<String, String> readableData = new HashMap<>();
        for (int i = 0; i < data.get(0).size(); i++) {
            String val;
            if (data.get(1).get(i) == null) {
                val = "0";
            } else {
                val = (String) data.get(1).get(i);
            }
            readableData.put((String) data.get(0).get(i), val);
        }
        return readableData;
    }

}

