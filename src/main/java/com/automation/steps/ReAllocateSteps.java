package com.automation.steps;

import com.automation.pages.AddressEntryScreen;
import com.automation.pages.ItemListScreen;
import com.automation.pages.MainScreen;
import com.automation.pages.ScannerScreen;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;

public class ReAllocateSteps {

	protected final AppiumDriver<MobileElement>driver;
	 String AllOCATEBARCODE="000077";
	 String REAllOCATEBARCODE ="000099";
	 String ITEMLIST="WL408534505";
	MainScreen main;
	ScannerScreen screen;
	ItemListScreen list;
	AddressEntryScreen entryscreen;
	
	public ReAllocateSteps(){
		this.driver= Hook.getDriver();
	}
	
	@When("^The user click on ReAllcoate Item$")
	public void userTapOnReAllocate(){
		main = new MainScreen(driver);
		main.tapOnReAllocate();
	}
	
	@And("^The user selects the items to reallocate$")
	public void userSelectsList(){
		list = new ItemListScreen(driver);
		list.clickCheckBoxes(0);
		list.clickCheckBoxes(1);
		list.clickCheckBoxes(2);	
	}
	@And("^The user click on change location$")
	public void userChangeLocation(){
		list = new ItemListScreen(driver);
		list.clickChangeLocation();
	}
	
	@When("^The user scan new shelf Allocation barcode$")
	public void userScansReAllocationBarcode(){
		screen = new ScannerScreen(driver);
		screen.sendBarcode(REAllOCATEBARCODE);
	}
	
	@Then("^The user allocate new storage$")
	public void userAllocateNewStorage(){
		screen = new ScannerScreen(driver);
		screen.clickAllocateNewStotage();
	}
	
	@When("^The user ReAllocate Barcode manually$")
	public void reAllocateShelfManually(){
		screen = new ScannerScreen(driver);
		screen.clickOnAddIcon();
		screen.enterBarcodeText(REAllOCATEBARCODE);
		screen.clickBarcodeTextYes();
	}
}
