package com.automation.runner;


import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;


@RunWith(Cucumber.class)

@CucumberOptions(strict = false, features = "Features", tags={"@BookInScanProcess"},
format = { "pretty",
"json:target/cucumber.json",
"html:target/cucumber-html-report",
"com.cucumber.listener.ExtentCucumberFormatter:target/report.html",}, glue = { "com.automation.steps" })

public class TestRunner {
/*
	@BeforeClass ,json:target/cucumber.json
	public static void startAppium(){
		AppiumServerController server = new AppiumServerController();
	    server.startServer();
	}
	
	@AfterClass
	public static void stopAppium() throws InterruptedException{
		AppiumServerController server = new AppiumServerController();
		server.stopServer();
	}*/
}

// @BookInScanProcess,@BookInManualProcess, @LocalCollectScanProcess, @LocalCollectManualProcess,@AllocateScanProcess,@AllocateManualProcess,@AllocateNonBarcodedItems,@ReAllocateScan,@ReAllocateManual,@BookOutScanProcess,@BookOutManulProcess,@BookOutSearchAddress